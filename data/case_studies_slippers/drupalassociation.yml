title: Drupal Association
file_name: drupalassociation
canonical_path: /customers/drupalassociation/
cover_image: /images/blogimages/drupalassoc_cover.jpg
cover_title: |
  Drupal Association eases entry for new committers, speeds implementations
cover_description: |
  Adopting GitLab as The DevOps Platform enabled greater contribution and collaboration.
twitter_image: /images/case_study_logos/drupalassoclogo.png
twitter_text: Learn how @drupalassoc enabled contribution and collaboration with GitLab
customer_logo: /images/case_study_logos/drupalassoclogo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Software
customer_location: Drupal.org
customer_solution: GitLab SaaS Ultimate
active_contributors: |
  120,000+
customer_overview: |
  Implementation of GitLab placed Drupal platform development in the mainstream of open-source evolution, while better supporting the contribution efforts of a wider range of the Drupal user community. 
customer_challenge: |
  Reduce obstacles to fast-paced development and community contributions
key_benefits: >-

  
    Faster implementations 

  
    Greater user involvement 

  
    Enhanced collaboration
customer_stats:
  - stat: 3x faster 
    label: Implementation of CI time reduced from 90 days to 30 days
  - stat: 6x rate 
    label: Frequency increased from 2-3 improvements released every 6 month, now improvements released monthly with every GitLab update 
  - stat: 10% higher
    label: Drupal core contributions up 10% since transitioning to GitLab Merge Requests
customer_study_content:
  - title: the customer
    subtitle: Drupal evolves with open-source to grow its platform
    content: >-
    
  
       The Drupal Association, steward of the free Drupal web content management system, stands as one of the most remarkable success stories of the open-source software era. Drupal serves as the capable backend framework for an estimated 13% of the top 10,000 websites worldwide, with users including the Fortune 50 companies, government, and higher education and many, many more. From website development to advanced content management and more, the platform has evolved, while relying on the innovation, know-how, and dedication of open-source contributors.  As of early 2021, the Drupal community included 121,000 active contributors. The Drupal Association enables thousands of websites to upgrade to use innovative and stable modules at an increasingly rapid pace.
  - title: the challenge
    subtitle:  Moving forward, reducing obstacles for new contributors
    content: >-
    

        Drupal had its 20th anniversary in 2021. As a veteran open source project, it actually predates git, let alone hosted collaboration platforms like GitLab or GitHub, or CI services like Travis. Like many of the projects of this era, Drupal built its own bespoke git backend, bespoke code collaboration tools, and bespoke continuous integration systems.  Well into 2020, the Drupal ecosystem still primarily relied on patches submitted to Drupal.org's custom issue tracker, and the bespoke DrupalCI system for integration testing. At the same time, Drupal as a product evolved in complexity and capability - making it an industry-leader in building ambitious digital experiences - but exposing the flaws and friction points of the aging collaboration tools. 


        The Drupal.org development team realized that, for Drupal to continue to thrive as an open-source platform, they needed to increase focus on their beginner experience, facilitating the continual growth of an essential and vibrant contributor community. New contributors needed to be able to quickly take part in frictionless development efforts. Drupal also needed end-to-end, browser-based management to give new users the ability to manage code contributions and workflow- and to empower users around the globe without access to powerful machines the tools they need to continue to contribute.  Achieving these goals was essential to putting the  focus back on what makes the Drupal project successful: empowered and energized contributors, focused on innovation, and free of friction. 



  - title: the solution
    subtitle:  GitLab opens up access and collaboration for more developers
    content: >-

        To achieve their new goals, the team looked to leverage an open collaboration and DevOps platform that would offer [end-to-end support](/solutions/devops-platform/). The Drupal.org engineering team was faced with the choice of continuing to maintain custom collaboration tools, or moving to a well supported, modern solution. During a multi-year evaluation process, the team evaluated  GitLab, GitHub and Bitbucket, and ultimately went with GitLab as the collaboration and DevOps platform of choice to support their open-source project because it offered “that modernized, feature-rich contribution workflow that developers expect, as well as allowing us to run our own self-hosted instance, which was crucial to configuring our new tools to meet Drupal's very open collaboration style” noted Timothy Lehnen, Chief Technology Officer at the Drupal Association. "After conducting an independent evaluation of multiple collaboration platforms including Github, Atlassian BitBucket, and GitLab - GitLab emerged as a clear winner do to its commitment to co-operation and collaboration with the Drupal project and the [open source community](/solutions/open-source/),” continued Lehnen. Adopting GitLab also provided a ready leg-up for the many contributors already familiar with Git, and that enabled them to recruit more people into the Drupal Community. This modernization also allowed the team to benefit from add-ons created by the ecosystem of tooling providers who extend GitLab's functionality, including Drupal service providers who had already adopted GitLab for their own internal work.
      
        “With GitLab, Drupal contributors have the tools they need - without the barrier of learning a full development stack that isn't  actually relevant to their domain,” said Lehnen, “It just makes it easier to collaborate on projects both for coders and non-coders.” 

        Community members are now able to employ a browser interface that supports the full end-to-end process of making a contribution. [GitLab CI](/stages-devops-lifecycle/continuous-integration/) supports a range of new workflows for the Drupal project. Both coders and non-coders alike  can use GitLab's WebIDE to support writing better code comments and in-code doc-blocks. A variety of users, including non-coders, such as product managers, accessibility engineers, content editors, and project managers, are now integrated into the overall workflow. This integration between coders and non-coders enables unprecedented collaboration across Drupal. 
      
        Use of GitLab also coincides with the project’s first implementations of Kubernetes clusters, as GitLabCI runners are being made available in beta to select projects in the Drupal ecosystem, and soon in wider release. This helps the Drupal project move from a centralized CI model, to one where project maintainers can implement their CI workflows of choice - innovating faster. As one example, Drupal is expanding beyond just a php project, and now publishes several javascript components to NPM, via GitLab CI/Pipelines. 
       
        
  - title: the results
    subtitle:   Enhanced efficiency and expanded capabilities
    content: >-

        Implementation has helped widely grow the capabilities of the Drupal community. GitLab is allowing more Drupal users to be more comfortable submitting fixes against the Drupal core. “We are seeing 400+ merge requests in the Drupal ecosystem per month since the change, and about a 50% close rate within the month,” said Lehnen.  Since the transition to merge requests, contribution to Drupal core itself is up by around 10%. Contributor resources now are applied more efficiently as the WebIDE saves contributor community members significant time configuring and setting up jobs. “You can make an end-to-end contribution to Drupal with just your browser. You do not need the command line. You do not need a local development environment,” added Lehnen. 

    
        “Now, contributors don’t need to learn how to be a local Drupal developer in order to review new features for accessibility concerns. The ability to contribute without setting up complex local development environments has made it easier for everyone from our accessibility maintainers, documentation editors, product managers, and others to review and comment on the work of our developers.” said Lehnen.  

    
        Meanwhile, the Drupal core team has come very close to 100% test coverage for the Drupal core, and will be able to expand that coverage to new parts of the codebase much more easily with GitLabCI. They’ve achieved primary goals like increased developer satisfaction for the Drupal contributor ecosystem and dramatically reduced dependence on the internal team for contributor feature development. Contributions have been expanded to include the first JavaScript packages to be added to the mostly-PHP project.     

    
        Going forward, further GitLab implementations will expand capabilities of the Drupal credit system, which highlights contributors’ efforts and innovations, providing useful metadata to help better understand how the Drupal community works. “Drupal leads the open source world in measuring and understanding our contribution ecosystem. This is one element of our bespoke tools that we'd love to keep, so we're thrilled that the GitLab team is supporting the idea of bringing this feature to GitLab. That will let the rest of the open source community benefit from these community health metrics as well,” said Lehnen.



       

        

    
  
        ## Learn more about GitLab solutions
    
  
        [Open Source with GitLab](/solutions/open-source/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: You do not need any tools other than your browser in order to contribute, and not just in our module ecosystem, but even to Drupal core itself. That was not possible in our project prior to the move to GitLab, and that's a really big deal.
    attribution: Timothy Lehnen
    attribution_title: CTO, Drupal Association









